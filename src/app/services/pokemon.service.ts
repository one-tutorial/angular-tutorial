import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) { }

  public getData(name: string | null) {
    if (name) {
      return this.http.get(`https://api.pokemontcg.io/v2/cards`, {
        params: {
          pageSize: 10,
          q: `name:${name}`
        },
      })
    } else {
      return this.http.get(`https://api.pokemontcg.io/v2/cards`, {
        params: {
          pageSize: 10,
        },
      })
    }

  }

  public getDataSet(setName: string | null) {
    if (setName) {
      return this.http.get(`https://api.pokemontcg.io/v2/sets`, {
        params: {
          pageSize: 10,
          q: `name:${setName}`
        },
      })
    } else {
      return this.http.get(`https://api.pokemontcg.io/v2/sets`, {
        params: {
          pageSize: 10,
        },
      })
    }
  }
}

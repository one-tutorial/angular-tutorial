import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./layouts/main/main.module').then(m => m.MainModule) },
  { path: 'transaction', loadChildren: () => import('./layouts/transaction/transaction.module').then(m => m.TransactionModule) },
  { path: 'pokemon', loadChildren: () => import('./layouts/pokemon/pokemon.module').then(m => m.PokemonModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

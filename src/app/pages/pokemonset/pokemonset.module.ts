import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonsetComponent } from './pokemonset.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: PokemonsetComponent, }
]



@NgModule({
  declarations: [
    PokemonsetComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class PokemonsetModule { }

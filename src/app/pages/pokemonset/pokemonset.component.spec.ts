import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonsetComponent } from './pokemonset.component';

describe('PokemonsetComponent', () => {
  let component: PokemonsetComponent;
  let fixture: ComponentFixture<PokemonsetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonsetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

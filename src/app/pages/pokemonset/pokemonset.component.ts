import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemonset',
  templateUrl: './pokemonset.component.html',
  styleUrls: ['./pokemonset.component.scss']
})
export class PokemonsetComponent implements OnInit {

  pokemonset: any;

  myForm = this.fb.group({
    name: [''],
  });

  constructor(
    private ps: PokemonService,
    private fb: FormBuilder

  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    let keyword = this.myForm.get('name')?.value;
    this.ps.getDataSet(keyword).subscribe({
      next: (response: any) => {
        this.pokemonset = response
        console.log(response);

      }, error: error => {
        console.log(error);

      }, complete: () => {

      }
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {

  pokemon: any;

  myForm = this.fb.group({
    name: [''],
  });

  constructor(
    private ps: PokemonService,
    private fb: FormBuilder

  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    let keyword = this.myForm.get('name')?.value;
    this.ps.getData(keyword).subscribe({
      next: (response: any) => {
        this.pokemon = response
        console.log(response);

      }, error: error => {
        console.log(error);

      }, complete: () => {

      }
    });
  }

}

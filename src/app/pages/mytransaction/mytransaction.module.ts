import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MytransactionComponent } from './mytransaction.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: MytransactionComponent }
];




@NgModule({
  declarations: [
    MytransactionComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class MytransactionModule { }

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactiveformsample',
  templateUrl: './reactiveformsample.component.html',
  styleUrls: ['./reactiveformsample.component.scss']
})
export class ReactiveformsampleComponent implements OnInit {

  /* myForm = new FormGroup({
    myNumber: new FormControl(''),
    myRandomNumber: new FormControl(''),
  }); */

  myForm = this.fb.group({
    myNumber: ['', Validators.required],
    myRandomNumber: ['', Validators.required],
  });


  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.myForm.value);
  }

  generateRandom() {
    let randomNumber = Math.random();
    this.myForm.get('myRandomNumber')?.setValue(randomNumber);
  }

}

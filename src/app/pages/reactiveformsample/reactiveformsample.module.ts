import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveformsampleComponent } from './reactiveformsample.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: ReactiveformsampleComponent }
];



@NgModule({
  declarations: [
    ReactiveformsampleComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ReactiveformsampleModule { }

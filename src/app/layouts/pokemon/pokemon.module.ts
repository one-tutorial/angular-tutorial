import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonComponent } from './pokemon.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: PokemonComponent, children: [
      { path: 'cards', loadChildren: () => import('../../pages/pokemon/pokemon.module').then(m => m.PokemonModule) },
      { path: 'sets', loadChildren: () => import('../../pages/pokemonset/pokemonset.module').then(m => m.PokemonsetModule) },
    ]
  }
];



@NgModule({
  declarations: [
    PokemonComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PokemonModule { }

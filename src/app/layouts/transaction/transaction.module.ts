import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionComponent } from './transaction.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: TransactionComponent, children: [
      { path: '', loadChildren: () => import('../../pages/mytransaction/mytransaction.module').then(m => m.MytransactionModule) },
    ]
  }
];



@NgModule({
  declarations: [
    TransactionComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class TransactionModule { }

import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpResponse, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
    constructor() { } intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        /* 
                const idToken = (sessionStorage.getItem('auth_token') ? sessionStorage.getItem('auth_token') : null);
                if (idToken) {
                    request = request.clone({
                        headers: request.headers.set('Authorization', 'Bearer ' + idToken)
                    });
                }
         */


        request = request.clone({
            headers: request.headers.set('X-Api-Key', '') // get API key by registering at https://dev.pokemontcg.io/
        });

        /* 
                request = request.clone({
                    headers: request.headers.set('X-MESSAGE', 'HELLO WORLD')
                });
         */

        console.log(request);
        return next.handle(request);
    }
}